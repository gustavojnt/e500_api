const fetch = require("node-fetch");
const { bearer } = require("../data/api.json");

const { Headers } = fetch;

export const authHeaders = new Headers({
  authorization: `Bearer ${bearer}`,
});

const url = new URL("https://api.twitter.com/2/tweets/search/recent");
const urlDetails = new URL("https://api.twitter.com/2/tweets");

export const fetchRecentTweets = async (hashtag = "") => {
  try {
    url.search = new URLSearchParams({
      query: hashtag,
    });

    const res = await fetch(url, { headers: authHeaders });
    const tweets = await res.json();

    if (tweets?.meta?.result_count === 0) {
      throw new Error("not_found");
    }

    return { data: tweets.data };
  } catch ({ message }) {
    throw new Error(`fn->fetchRecentTweets()->${message}`);
  }
};

export const fetchTweetsDetails = async (ids = "") => {
  try {
    urlDetails.search = new URLSearchParams({
      ids,
      expansions: "author_id",
      "user.fields": "profile_image_url,username",
    });

    const res = await fetch(urlDetails, { headers: authHeaders });
    const tweets = await res.json();

    if (tweets?.meta?.result_count === 0) {
      throw new Error("not_found");
    }

    if (tweets?.includes?.users.length) {
      const tweetAuthorKeyMap = new Map();
      tweets.includes.users.forEach((user) => {
        tweetAuthorKeyMap.set(user.id, user);
      });

      const data = [];
      tweets.data.forEach((tweet) => {
        const user = tweetAuthorKeyMap.get(tweet.author_id);
        if (user) {
          data.push({
            id: tweet.id,
            username: user.username,
            post: tweet.text,
            avatar: user.profile_image_url,
          });
        }
      });

      return { data };
    }

    return { data: tweets.data };
  } catch ({ message }) {
    throw new Error(`fn->fetchTweetsDetails()->${message}`);
  }
};

export const ids = (obj = { data: [] }) => {
  if (!obj.data) {
    throw new Error("fn->ids()->invalid_function_params");
  }
  return obj.data.map(({ id }) => id).toString();
};

export const tryResponse = async (handler, res) => {
  try {
    const data = await handler();
    return res.json(data);
  } catch ({ message: msg }) {
    if (msg.includes("not_found")) {
      return res.status(404).json({ data: [], err: msg });
    }

    return res.status(400).json({ error: msg });
  }
};
