const {
  fetchRecentTweets,
  fetchTweetsDetails,
  ids,
  tryResponse,
} = require("../utils");

module.exports = async (req, res) => {
  tryResponse(async () => {
    const tweets = await fetchRecentTweets("#SuvFiat");
    const tweetsDetails = await fetchTweetsDetails(ids(tweets));
    return tweetsDetails;
  }, res);
};
